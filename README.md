# STC File Processor Assignment

## Requirements
There is a file with a list of numbers with a maximum value of 9999999. However, there are
duplicate numbers in the file. Also imagine that this program is running within an abnormally
constrained memory and CPU environment. Write a program that will return the duplicate numbers
via a REST service, i.e. http://localhost:8000/files. Attempt to make the solution:
- Return a JSON. Response should be a JSON containing
    - the duplicate numbers
    - number of duplications for each number
- As memory efficient and as you can.
- As performant as you can.
- Use a build technology such as maven or gradle.
- The solution should run, with test data of your own creation, as a unit test(s), as part of the
build.
- While you are maximizing the efficiency of memory and performance, also try balance this
requirement against best use of OO design principals where appropriate.
- Use your judgement in following best practice coding standards.
- Accompany your solution with documentation describing:
    - How to build.
    - Design decisions.
    - Test plan and results.
    - Performance assessment.

## How to Build & Run
To run the application, after cloning repository, move to project directory and run `mvn clean spring-boot:run`. Unit tests can be run with `mvn clean test`. To process files with larger size than `stc.max-file-size`, application assumes Redis is accessible on running machine on `port:6379`.

## Design Decisions
Application is developed in Spring Boot. Following are selected options:
- Undertow for embedded servlet container because of performance reasons. Comparisons can be found here: https://examples.javacodegeeks.com/enterprise-java/spring/tomcat-vs-jetty-vs-undertow-comparison-of-spring-boot-embedded-servlet-containers/
- Jackson as JSON convertion library  because of performance reasons. Comparisons can be found here: https://blog.takipi.com/the-ultimate-json-library-json-simple-vs-gson-vs-jackson-vs-json/

### GET /files/{filename}/duplicates
Endpoint checks existence and file size of a file named `filename` in project directory. Based on maximum file size (stc.max-file-size), application chooses from two different strategies to process numbers in file: store in Redis or store in a List. 
- RedisProcessor: Stores numbers and counts as key-value tuples in Redis. INCR command is invoked for each number. Number count is retrieved from values.
- ListProcessor: Stores numbers in an ArrayList. When addition operations are completed, sorts list naturally and counts each number occurrence. 

### POST /files
Endpoints creates a new file with given parameters. Maximum values are 10M for token value and token count in a file.

### Further Improvements
- RedisProcessor may call INCR and SCAN commands in threads to get faster.

## Performance Results
JMeter file is located under src/test/jmeter folder. Application was tested with 128MB memory `(-XmX128m)` under following conditions:
- File containing 10K random duplicate entries & 2000 requests & Loop count = Forever (Manually stopped after 5m)
- File containing 10K random duplicate entries & 2000 requests & Loop count = 1  (Took ~12s)
- File containing 100K random duplicate entries & 2000 requests & Loop count = 1  (Took 2m45s)
- File containing 1M random duplicate entries & single request & Loop count = 1 (Took 3m)



