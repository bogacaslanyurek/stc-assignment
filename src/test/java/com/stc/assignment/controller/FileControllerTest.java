package com.stc.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stc.assignment.dto.StcFileRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class FileControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenFile_whenFindDuplicates_thenShouldMapProperly() throws Exception {
        this.mockMvc.perform(get("/files/test/duplicates")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(
                        "[{\"number\":\"2\",\"count\":\"2\"},{\"number\":\"3\",\"count\":\"3\"},{\"number\":\"4\",\"count\":\"4\"}]"
                ));
    }


    @Test
    public void givenFileParams_whenNewFile_thenShouldCreate() throws Exception {
        StcFileRequest request = new StcFileRequest(100, 100, "test-file", true);

        this.mockMvc.perform(post("/files")
                .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(request)))
                .andDo(print()).andExpect(status().isOk());

    }
}