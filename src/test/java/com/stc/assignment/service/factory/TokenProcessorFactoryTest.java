package com.stc.assignment.service.factory;

import com.stc.assignment.service.processor.ProcessorService;
import com.stc.assignment.service.processor.impl.ListProcessorServiceImpl;
import com.stc.assignment.service.processor.impl.RedisProcessorServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "stc.processor.max-file-size=1000",
})
public class TokenProcessorFactoryTest {

    @Value("${stc.processor.max-file-size:1000}")
    private String maxFileSize;

    @Autowired
    private TokenProcessorFactory tokenProcessorFactory;

    @Test
    public void givenProcessorServices_whenMaxFileSizeExceeded_thenShouldReturnRedis() {
        ProcessorService processorService = tokenProcessorFactory.getProcessorService(2000L);
        assertThat(processorService).isInstanceOf(RedisProcessorServiceImpl.class);
    }

    @Test
    public void givenProcessorServices_whenMaxFileSizeNotExceeded_thenShouldReturnList() {
        ProcessorService processorService = tokenProcessorFactory.getProcessorService(500L);
        assertThat(processorService).isInstanceOf(ListProcessorServiceImpl.class);
    }
}