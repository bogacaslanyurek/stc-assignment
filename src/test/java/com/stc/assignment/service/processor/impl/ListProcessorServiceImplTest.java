package com.stc.assignment.service.processor.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ListProcessorServiceImplTest {

    @Test
    public void givenNoTokens_whenAddToken_thenShouldHaveOne() {
        ListProcessorServiceImpl service = new ListProcessorServiceImpl();
        service.processToken("123456");
        assertThat(service.getTokensList()).containsExactly(123456);
    }

    @Test
    public void givenTokens_whenGetDuplicate_thenShouldReturn() {
        ListProcessorServiceImpl service = new ListProcessorServiceImpl();
        service.processToken("2");
        service.processToken("2");
        service.processToken("1");
        service.processToken("3");
        service.processToken("3");
        service.processToken("3");

        Map<String, String> duplicateTokenCount = service.getDuplicateTokenCount();

        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("2", "2");
        expectedMap.put("3", "3");

        assertThat(duplicateTokenCount).isEqualTo(expectedMap);
    }
}