package com.stc.assignment.service.processor.impl;

import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.RedisClient;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RedisProcessorServiceImplTest {

    private final RedisClient client = mock(RedisClient.class);

    private final StatefulRedisConnection conn = mock(StatefulRedisConnection.class);

    private final RedisCommands redis = mock(RedisCommands.class);

    private final KeyScanCursor<String> cursor = mock(KeyScanCursor.class);

    private final KeyScanCursor<String> finishedCursor = mock(KeyScanCursor.class);

    @Test
    public void givenToken_whenProcess_thenIncrement() {
        when(client.connect()).thenReturn(conn);
        when(conn.sync()).thenReturn(redis);

        RedisProcessorServiceImpl service = new RedisProcessorServiceImpl(client);
        service.processToken("1");

        verify(redis).incr(service.getIdentifier() + ":1");
    }

    @Test
    public void givenTokens_whenFindDuplicate_thenShouldReturn() {
        when(client.connect()).thenReturn(conn);
        when(conn.sync()).thenReturn(redis);

        RedisProcessorServiceImpl service = new RedisProcessorServiceImpl(client);
        String identifier = service.getIdentifier();

        when(redis.scan(any(ScanArgs.class))).thenReturn(cursor);
        when(cursor.getKeys()).thenReturn(Arrays.asList(identifier + ":1", identifier + ":2", identifier + ":3"));
        when(finishedCursor.isFinished()).thenReturn(true);
        when(redis.scan(any(KeyScanCursor.class))).thenReturn(finishedCursor);

        Map<String, String> redisMockMap = new HashMap<>();
        redisMockMap.put("1", "1");
        redisMockMap.put("2", "2");
        redisMockMap.put("3", "3");

        when(redis.get(identifier + ":1")).thenReturn(redisMockMap.get("1"));
        when(redis.get(identifier + ":2")).thenReturn(redisMockMap.get("2"));
        when(redis.get(identifier + ":3")).thenReturn(redisMockMap.get("3"));

        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("2", "2");
        expectedMap.put("3", "3");

        Map<String, String> duplicateTokenCount = service.getDuplicateTokenCount();
        assertThat(duplicateTokenCount).isEqualTo(expectedMap);

    }
}