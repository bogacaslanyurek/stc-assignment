package com.stc.assignment.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class DuplicateNumber {

    private final String number;

    private final String count;

}
