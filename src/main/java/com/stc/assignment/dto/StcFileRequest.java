package com.stc.assignment.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Max;

@RequiredArgsConstructor
@Getter
public class StcFileRequest {

    @Max(10000000L)
    private final Integer tokenCount;

    @Max(10000000L)
    private final Integer tokenMaxValue;

    private final String fileName;

    private final Boolean allowDuplicates;

}
