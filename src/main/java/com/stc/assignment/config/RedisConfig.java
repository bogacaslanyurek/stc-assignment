package com.stc.assignment.config;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedisConfig {

    @Bean
    public RedisClient redisClient(){
        return RedisClient
                .create(RedisURI.builder().withHost("localhost").withPort(6379).build());
    }

}
