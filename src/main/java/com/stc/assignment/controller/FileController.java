package com.stc.assignment.controller;

import com.stc.assignment.dto.DuplicateNumber;
import com.stc.assignment.dto.StcFileRequest;
import com.stc.assignment.service.FileGeneratorService;
import com.stc.assignment.service.FileProcessorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class FileController {

    private final FileProcessorService fileProcessorService;

    private final FileGeneratorService fileGeneratorService;

    @GetMapping("/files/{filename}/duplicates")
    public List<DuplicateNumber> findDuplicates(@PathVariable("filename") String fileName) throws IOException {
        Map<String, String> duplicateMap = fileProcessorService.getDuplicatesFromFile(fileName);

        return duplicateMap.entrySet().stream()
                .map(e -> new DuplicateNumber(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    @PostMapping("/files")
    public void createFile(@RequestBody StcFileRequest request) {
        fileGeneratorService.generateFile(request.getFileName(), request.getTokenCount(), request.getTokenMaxValue(), request.getAllowDuplicates());
    }

}
