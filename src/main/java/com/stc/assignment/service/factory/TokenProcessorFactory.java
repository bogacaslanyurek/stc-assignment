package com.stc.assignment.service.factory;

import com.stc.assignment.service.processor.ProcessorService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class TokenProcessorFactory {

    @Value("${stc.processor.max-file-size:1000}")
    private String maxFileSize;

    @Resource(name = "list")
    private ProcessorService listProcessorService;

    @Resource(name = "redis")
    private ProcessorService redisProcessorService;

    public ProcessorService getProcessorService(Long fileSize) {
        if (Long.valueOf(maxFileSize).compareTo(fileSize) > 0) {
            return listProcessorService;
        } else return redisProcessorService;
    }


}
