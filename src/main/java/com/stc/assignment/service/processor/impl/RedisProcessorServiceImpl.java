package com.stc.assignment.service.processor.impl;

import com.stc.assignment.service.processor.ProcessorService;
import io.lettuce.core.KeyScanCursor;
import io.lettuce.core.RedisClient;
import io.lettuce.core.ScanArgs;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class saves tokens into redis.
 */
@Component("redis")
@Slf4j
@Scope(scopeName = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RedisProcessorServiceImpl implements ProcessorService {

    private final RedisCommands<String, String> redis;

    private final String identifier = UUID.randomUUID().toString(); // instance identifier


    RedisProcessorServiceImpl(RedisClient redisClient) {
        this.redis = redisClient.connect().sync();
    }

    @Override
    public void processToken(String token) {
        this.redis.incr(identifier + ":" + token);
    }

    /**
     * This method loops all related keys using SCAN method and
     * puts each number into an hash map with number as key and count as value.
     */
    @Override
    public Map<String, String> getDuplicateTokenCount() {
        Map<String, String> duplicateTokenMap = new HashMap<>();
        log.debug("Starting Redis SCAN");

        long start = System.currentTimeMillis();
        KeyScanCursor<String> cursor = redis.scan(ScanArgs.Builder.limit(50).match(identifier + ":*"));
        do {
            for (String key : cursor.getKeys()) {
                String value = redis.get(key);
                if (Integer.valueOf(value).compareTo(1) > 0) {
                    String[] split = key.split(":");
                    duplicateTokenMap.put(split[1], value);
                }
            }
            cursor = redis.scan(cursor);
        } while (!cursor.isFinished());

        long finish = System.currentTimeMillis();
        log.debug("Finished Redis SCAN in {} milliseconds", (finish - start));
        return duplicateTokenMap;
    }


    public String getIdentifier() {
        return identifier;
    }
}

