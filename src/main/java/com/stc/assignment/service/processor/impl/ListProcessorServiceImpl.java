package com.stc.assignment.service.processor.impl;

import com.stc.assignment.service.processor.ProcessorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.*;


/**
 * This class saves tokens into an array list.
 */
@Component("list")
@Scope(scopeName = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Slf4j
public class ListProcessorServiceImpl implements ProcessorService {

    private final List<Integer> listOfTokens = new ArrayList<>();

    public List<Integer> getTokensList() {
        return listOfTokens;
    }

    @Override
    public void processToken(String line) {
        listOfTokens.add(Integer.valueOf(line));
    }

    /**
     * This method  sorts list by natural order before processing. After it loops through list,
     * counts each number's occurrence until a new number is found and puts each number into an hash map
     * with number as key and count as value.
     */
    @Override
    public Map<String, String> getDuplicateTokenCount() {
        listOfTokens.sort(Comparator.naturalOrder());

        Map<String, String> duplicateCount = new HashMap<>();

        int currentNumber = -1;
        int count = 0;

        long start = System.currentTimeMillis();
        log.debug("Started looping list");
        for (Integer listOfNumber : listOfTokens) {
            if (currentNumber == listOfNumber) {
                count++;
            } else {
                if (count > 1) {
                    duplicateCount.put(String.valueOf(currentNumber), String.valueOf(count));
                }
                currentNumber = listOfNumber;
                count = 1;
            }
        }

        duplicateCount.put(String.valueOf(currentNumber), String.valueOf(count));
        long finish = System.currentTimeMillis();
        log.debug("Finished looping list in {} milliseconds", finish - start);
        return duplicateCount;
    }
}
