package com.stc.assignment.service.processor;

import java.util.Map;

public interface ProcessorService {

    void processToken(String line);

    Map<String,String> getDuplicateTokenCount();

}
