package com.stc.assignment.service;

import com.stc.assignment.service.factory.TokenProcessorFactory;
import com.stc.assignment.service.processor.ProcessorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class FileProcessorService {

    private final TokenProcessorFactory factory;

    public Map<String, String> getDuplicatesFromFile(String fileName) throws IOException {
        ProcessorService processorService;
        Path path = Paths.get(fileName);

        try {
            Long fileSize = Files.size(path);
            processorService = factory.getProcessorService(fileSize);
        } catch (IOException e) {
            log.error("Could not get file size {}", fileName, e);
            throw e;
        }

        try (BufferedReader bufferedReader = Files.newBufferedReader(path, Charset.forName("US-ASCII"))) {
            String token;
            long start = System.currentTimeMillis();
            log.debug("Started reading file", fileName);
            while ((token = bufferedReader.readLine()) != null) {
                processorService.processToken(token);
            }
            long finish = System.currentTimeMillis();
            log.debug("Finished reading file {} in {} milliseconds", fileName, finish - start);
        }
        return processorService.getDuplicateTokenCount();
    }


}
