package com.stc.assignment.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

@Component
@Slf4j
public class FileGeneratorService {

    public void generateFile(String fileName, Integer tokenCount, Integer tokenMaxValue, Boolean allowDuplicates) {
        Charset charset = Charset.forName("US-ASCII");

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName), charset)) {
            int i = 0;
            while (i < tokenCount) {
                if (allowDuplicates) {
                    Random random = new Random();
                    writer.write(String.valueOf(random.nextInt(tokenMaxValue)));
                } else {
                    writer.write(String.valueOf(i));
                }
                writer.newLine();
                i++;
            }
        } catch (IOException x) {
            log.error("Exception occurred while generating file", x);
        }
    }
}
