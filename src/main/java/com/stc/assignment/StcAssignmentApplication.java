package com.stc.assignment;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StcAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(StcAssignmentApplication.class, args);
    }
}
